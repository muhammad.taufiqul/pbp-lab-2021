import 'package:flutter/material.dart';

void main() => runApp(Lab6());

class Lab6 extends StatelessWidget {
  const Lab6({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Formulir pendaftaran';
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
          appBarTheme: AppBarTheme(
              backgroundColor: Colors.blueGrey.shade600,
              foregroundColor: Colors.white)),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const FormulirPendaftaran(),
      ),
    );
  }
}

class FormulirPendaftaran extends StatefulWidget {
  const FormulirPendaftaran({Key? key}) : super(key: key);

  @override
  _FormulirPendaftaranState createState() => _FormulirPendaftaranState();
}

class _FormulirPendaftaranState extends State<FormulirPendaftaran> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();
  final _biggerFont = const TextStyle(fontSize: 18);

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _formField('Nama Lengkap'),
          _formField('NPM'),
          _formField('email'),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Submit successful')));
                }
              },
              child: const Text('Submit'),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.blueGrey.shade600,
                  primary: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Column _formField(String judul) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          judul,
          style: _biggerFont,
        ),
        TextFormField(
          // The validator receives the text that the user has entered.
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Please fill this field';
            }
            return null;
          },
        )
      ],
    );
  }
}
