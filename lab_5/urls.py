from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('notes/<int:note_id>/', include([
        path('', views.get_note, name='get_note'),
        path('update', views.update_note, name='update_note'),
        path('delete', views.delete_note, name='delete_note'),
    ])),
]