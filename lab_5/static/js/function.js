$(document).ready(function(){
    var url_mask = $("#lab-2JsonLink").html();
    var editModal = null;
    $.updateTable(url_mask);

    $("tbody").on("click", "#buttonView", function () {
        var singleNoteModal = new bootstrap.Modal(document.getElementById('singleNoteModal'));
        var fieldContent = $.getNote($(this).attr("data-id"));
        $("#singleNoteModal .modal-title").text(fieldContent["title"]);
        $("#singleNoteModal .modal-from").text("From: " + fieldContent["from"]);
        $("#singleNoteModal .modal-to").text("To: " + fieldContent["to"]);
        $("#singleNoteModal .modal-message").text(fieldContent["message"]);
        singleNoteModal.show();
    });

    $("tbody").on("click", "#buttonEdit", function() {
        editModal = new bootstrap.Modal(document.getElementById('editModal'));
        var note_id = $(this).attr("data-id");
        var fieldContent = $.getNote(note_id);
        $("#inputTo").val(fieldContent["to"]);
        $("#inputFrom").val(fieldContent["from"]);
        $("#inputTitle").val(fieldContent["title"]);
        $("#inputMessage").val(fieldContent["message"]);
        $("#inputNoteId").text(note_id);
        editModal.show();
    });

    $("tbody").on("click", "#buttonDelete", function() {
        var deleteConfirmModal = new bootstrap.Modal(document.getElementById('deleteConfirmModal'));
        var note_id = $(this).attr("data-id");
        $("#deleteNoteId").text(note_id);
        deleteConfirmModal.show();
    });

    $("#editForm").submit(function (e) {
        e.preventDefault();
        var serializedData = $(this).serialize();
        var formSubmitUrl = "notes/" + $("#inputNoteId").text() + "/update";
        // make POST ajax call
        $.ajax({
            type: "POST",
            url: formSubmitUrl,
            data: serializedData,
            success: function (response) {
                $.updateTable(url_mask);
            },
            error: function (response) {
                alert(response["responseJSON"]["error"])
            }
        });
    });

    $("#deleteConfirmButton").submit(function (e) {
        e.preventDefault();
        var serializedData = $(this).serialize();
        var deleteUrl = "notes/" + $("#deleteNoteId").text() + "/delete";
        // make POST ajax call
        $.ajax({
            type: "POST",
            url: deleteUrl,
            data: serializedData,
            success: function (response) {
                $.updateTable(url_mask);
            },
            error: function (response) {
                alert(response["responseJSON"]["error"])
            }
        });
    });
});

$.extend({
    getNote: function(id) {
        var returnResponse = null;
        var url = "notes/" + id;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "JSON",
            async: false,
            success: function (response) {
                returnResponse = response;
            }
        });
        return returnResponse[0]["fields"];
    },

    updateTable: function(url_mask) {
        $.ajax({
            url: url_mask,
            type: "GET",
            dataType: "JSON",
            success: async function(response){
                // Destroy old table
                $("#noteTable tbody").html("");
                // Append content to table body
                for (var x of response) {
                    var fieldsObj = x["fields"];
                    var rowContent = "<tr>" +
                    "<td>" + fieldsObj["to"] + "</td>" +
                    "<td>" + fieldsObj["from"] + "</td>" +
                    "<td>" + fieldsObj["title"] + "</td>" +
                    "<td>" + fieldsObj["message"] + "</td>" +
                    `<td><button type="button" class="btn btn-secondary" style="width: 65px;" id="buttonView"` + "data-id=" + x["pk"] + ">View</button></td>" +
                    `<td><button type="button" class="btn btn-secondary" style="width: 65px;" id="buttonEdit"` + "data-id=" + x["pk"] + ">Edit</button></td>" +
                    `<td><button type="button" class="btn btn-secondary" id="buttonDelete"` + "data-id=" + x["pk"] + ">Delete</button></td>" +
                    "</tr>";
                    var rowFadeEffect = $(rowContent).hide();
                    $("#noteTable tbody").append(rowFadeEffect);
                    rowFadeEffect.fadeIn(500);
                    await sleep(100);
                }
            },
        });
    }
});

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}