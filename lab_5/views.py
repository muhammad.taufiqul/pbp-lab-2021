from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers
from .forms import NoteForm
from django.http import JsonResponse

# Create your views here.
def index(request):
    note = Note.objects.all()
    form = NoteForm()
    response = {'notes': note, "form": form}
    return render(request, 'lab5_index.html', response)

def get_note(request, note_id):
    is_ajax = request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
    if (is_ajax and request.method == 'GET'):
        note = Note.objects.filter(pk=note_id)
        data = serializers.serialize('json', note)
        return HttpResponse(data, content_type='application/json')

def update_note(request, note_id):
    is_ajax = request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

    # request should be ajax and method should be POST.
    if (request.method == 'POST' and is_ajax):
        noteInstance = Note.objects.get(pk=note_id)
        # get the form data
        form = NoteForm(request.POST, instance=noteInstance)
        # save the data and after fetch the object in instance
        if form.is_valid():
            instance = form.save()
            # serialize in new note object in json
            ser_instance = serializers.serialize('json', [ instance, ])
            # send to client side.
            return JsonResponse({}, status=200)
        else:
            # some form errors occured.
            return JsonResponse({"error": form.errors}, status=400)

    # some error occured
    return JsonResponse({"error": ""}, status=400)

def delete_note(request, note_id):
    is_ajax = request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'

    # request should be ajax and method should be POST.
    if (request.method == 'POST' and is_ajax):
        noteInstance = Note.objects.get(pk=note_id)
        # Delete object
        noteInstance.delete()
        return JsonResponse({}, status = 200)
    else:
        # some error occured
        return JsonResponse({"error": ""}, status=400)