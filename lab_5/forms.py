from typing import Text
from django.forms import ModelForm, TextInput, Textarea
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ('to', 'from', 'title', 'message')
        widgets = {'to': TextInput(attrs={'class': 'form-control border-dark', 'id': 'inputTo'}),
                   'from': TextInput(attrs={'class': 'form-control border-dark', 'id': 'inputFrom'}),
                   'title': TextInput(attrs={'class': 'form-control border-dark', 'id': 'inputTitle'}),
                   'message': Textarea(attrs={'class': 'form-control border-dark', 'id': 'inputMessage', 'rows': '6'})}