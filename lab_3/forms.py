from django.forms import ModelForm, DateInput
from lab_1.models import Friend

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ('name', 'npm', 'DOB')
        widgets = {'DOB': DateInput(attrs={'type': 'date'})}