## 1. Apakah perbedaan antara JSON dan XML?
Menurut observasi saya setelah mengerjakan lab 2 ini, format json berbentuk text dengan model seperti dictionary pada python sedangkan xml berbentuk seperti tree dimulai dari root sampai isi-isinya.

## 2. Apakah perbedaan antara HTML dan XML?
HTML digunakan untuk menampilkan suatu data dan tidak bisa menyimpan data dinamis sedangkan XML digunakan untuk memindahkan data yang dapat berubah-ubah.