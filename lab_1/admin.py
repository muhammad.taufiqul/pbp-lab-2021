from django.contrib import admin

# Register Friend model here
from .models import Friend
admin.site.register(Friend)