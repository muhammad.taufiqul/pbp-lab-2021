from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    note = Note.objects.all()
    response = {'notes': note}
    return render(request, 'lab2.html', response)

def xml(request):
    note = Note.objects.all()
    data = serializers.serialize('xml', note)
    return HttpResponse(data, content_type='application/xml')

def json(request):
    note = Note.objects.all()
    data = serializers.serialize('json', note)
    return HttpResponse(data, content_type='application/json')