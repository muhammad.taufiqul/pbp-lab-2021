from lab_1.views import index
from django.urls import path
from django.urls.resolvers import URLPattern
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('xml', views.xml, name='xml'),
    path('json', views.json, name='json')
]