from django.db import models
from django.db.models.fields import CharField

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=100)
    f_from = models.CharField(max_length=100, name='from')
    title = models.CharField(max_length=100)
    message = models.CharField(max_length=1000)